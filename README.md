# buran-nodemcu-updater

Buran updater module for NodeMCU devices

# Docker Run

1. Build

docker build -t buran-nodemcu-updater .

2. Run

docker run -d -it -p {SPECIFY_DOWNLOAD_PORT}:9000 -p {SPECIFY_SOCKET_PORT}:8080 -e DOWNLOAD_PATH={SPECIFY_DOWNLOAD_PATH} -e EVENT_SOCKET_URL={SPECIFY_SOCKET_UEL} buran-nodemcu-updater

Example:

docker run -d -it -p 1919:9000 -p 2020:8080 -e DOWNLOAD_PATH=http://192.168.104.199:1919 -e EVENT_SOCKET_URL=http://192.168.104.199:1111 buran-nodemcu-updater